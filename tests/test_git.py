import unittest
import os
import logging
from lu_semver.misc import Shell
from lu_semver.git import GitVersion, GitRepo
from lu_semver.scm import _branch_env

logging.basicConfig(level=logging.INFO, format="%(message)s")
log = logging.getLogger("lu")
# log.setLevel(logging.DEBUG)

os.environ['TERM'] = 'xterm-256color'


class SimpleTest(unittest.TestCase):
    def setUp(self):
        self.count = 0

    def tearDown(self):
        pass

    def loop_ver(self, path, answers, fmt="branch,count:pfx=dev"):
        for key, val in answers.items():
            ver = GitVersion(path=path, ver=key, fmt=fmt)
            self.assertEqual(str(ver), val)

    def test_no_tag_master(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        answers = {
            '@next': '0.1.1-dev.3',
            '@same': '0.1.0-dev.3',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf-dev.3'
        }
        self.loop_ver(r.path, answers)


    def test_no_tag_branch(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        r.checkout('feat')
        for i in range(3):
            r.add_commit()
        answers = {
            '@next': '0.1.1-feat.dev.6',
            '@same': '0.1.0-feat.dev.6',
            '@latest': 'latest-feat',
            'deadbeaf': 'deadbeaf-feat.dev.6'
        }
        self.loop_ver(r.path, answers)

    def test_no_tag_shallow_fetch(self):
        r = GitRepo()
        r.add_commit()
        for i in range(250):
            r.add_commit()
        r2 = GitRepo(url=r.path, depth=30)
        os.environ[_branch_env] = 'master'
        answers = {
            '@next': '0.1.1-dev.251',
            '@same': '0.1.0-dev.251',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf-dev.251'
        }
        self.loop_ver(r2.path, answers)
        del os.environ[_branch_env]

    def test_after_tag_master(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        r.add_tag('2.3.4')
        for i in range(4):
            r.add_commit()
        answers = {
            '@next': '2.3.5-dev.4',
            '@same': '2.3.4-dev.4',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf-dev.4'
        }
        self.loop_ver(r.path, answers)

    def test_after_tag_branch(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        r.add_tag('2.3.4')
        r.checkout('feat')
        for i in range(4):
            r.add_commit()
        answers = {
            '@next': '2.3.5-feat.dev.4',
            '@same': '2.3.4-feat.dev.4',
            '@latest': 'latest-feat',
            'deadbeaf': 'deadbeaf-feat.dev.4'
        }
        self.loop_ver(r.path, answers)

    def test_after_tag_shallow_fetch(self):
        r = GitRepo()
        r.add_commit()
        r.add_tag('2.3.4')
        for i in range(250):
            r.add_commit()
        r2 = GitRepo(url=r.path, depth=30)
        os.environ[_branch_env] = 'master'
        answers = {
            '@next': '2.3.5-dev.250',
            '@same': '2.3.4-dev.250',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf-dev.250'
        }
        self.loop_ver(r2.path, answers)
        del os.environ[_branch_env]

    def test_at_tag_master(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        r.add_tag('2.3.4')
        answers = {
            '@next': '2.3.4',
            '@same': '2.3.4',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf'
        }
        self.loop_ver(r.path, answers)

    def test_at_tag_branch(self):
        r = GitRepo()
        for i in range(3):
            r.add_commit()
        r.add_tag('2.3.4')
        r.checkout('feat')
        answers = {
            '@next': '2.3.4',
            '@same': '2.3.4',
            '@latest': 'latest-feat',
            'deadbeaf': 'deadbeaf'
        }
        self.loop_ver(r.path, answers)

    def test_at_tag_shallow_fetch(self):
        r = GitRepo()
        r.add_commit()
        for i in range(250):
            r.add_commit()
        r.add_tag('2.3.4')
        r2 = GitRepo(url=r.path, depth=30)
        os.environ[_branch_env] = 'master'
        answers = {
            '@next': '2.3.4',
            '@same': '2.3.4',
            '@latest': 'latest',
            'deadbeaf': 'deadbeaf'
        }
        self.loop_ver(r2.path, answers)
        del os.environ[_branch_env]

    def test_scm_extra(self):
        r = GitRepo()
        h = r.add_commit()
        ver = GitVersion(path=r.path, ver='@next', fmt='branch,count,scm')
        self.assertEqual(str(ver), '0.1.1-dev.1.git.' + h)
        ver = GitVersion(path=r.path, ver='@next', fmt='branch,count,extra', extra='gcc.7.6.0')
        # print('ver %s' % str(ver))
        self.assertEqual(str(ver), '0.1.1-dev.1.gcc.7.6.0')

    def no_test_demo(self):
        r = GitRepo()
        for i in range(2):
            r.add_commit()
        r.add_tag('1.0.0')
        r.checkout('feat1')
        for i in range(2):
            r.add_commit()
        r.checkout('master')
        for i in range(2):
            r.add_commit()
        r.add_tag('1.3.0')
        r.checkout('feat2')
        for i in range(3):
            r.add_commit()
        r.checkout('master')
        for i in range(3):
            r.add_commit()
        r.log()
        sh = Shell()
        sh('rm -rf /tmp/demo; cp -R %s /tmp/demo' % r.path)
