from setuptools import setup

pkgname = "lu-semver"
modname = pkgname.replace('-', '_')

setup(
    name=pkgname,
    # use_scm_version=True,
    use_scm_version={'write_to': 'src/__version__.py'},
    setup_requires=["setuptools_scm"],
    description="print semantic version for git projects",
    long_description=open("README.md", "r").read(),
    long_description_content_type="text/markdown",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development",
    ],
    url="https://gitlab.com/aanatoly/semver-tool",
    author="Anatoly Asviyan",
    author_email="anatoly@gmail.com",
    license="GPLv2",
    packages=[
        modname,
    ],
    package_dir={
        modname: 'src',
    },
    install_requires=[],
    extras_require={
        "dev": ["twine", "tox"]
    },
    entry_points={
        "console_scripts": [
            "lu-semver = %(modname)s.__main__:main" % {"modname": modname},
        ]
    },
    zip_safe=False,
)
