
SHELL := /bin/bash
PYTHON ?= python3

NAME := $(shell python setup.py --name | sed 's/-/_/g')
VER := $(shell python setup.py --version)
PY2 :=
PKG := dist/$(NAME)-$(VER)-$(if $(PY2),py2.)py3-none-any.whl

# Virtual Env, one per OS to avoid mess
env_distro = $(shell lsb_release -ds | sed -e 's/ /-/g' | tr '[:upper:]' '[:lower:]')
env_python = $(shell $(PYTHON) --version |& sed -e 's/ /-/g' | tr '[:upper:]' '[:lower:]')
env_py = $(shell $(PYTHON) --version |& sed -e 's/ //g' -e 's/\.[0-9]\+$$//' | tr '[:upper:]' '[:lower:]')
env_path = venv/$(env_distro)/$(env_python)
env_activate = $(env_path)/bin/activate
env_init = $(env_path)/.dev-init
$(info virtualenv: $(env_path))

ENVON = . $(env_activate)

.PHONY: init build install clean distclean test upload
.DEFAULT_GOAL := build

$(env_activate):
	virtualenv -p $(env_py) $(env_path)

all: $(env_activate)

init: $(env_init)
$(env_init): $(env_activate)
	$(ENVON); pip install .[dev]
	touch $@

build: $(PKG)
	@true

$(PKG): $(env_init)
	$(ENVON); python setup.py bdist_wheel $(if $(PY2),--universal)

install: $(env_init) $(PKG)
	$(ENVON); pip install $(PKG)

clean:
	rm -rf .eggs build/ dist/ $(NAME).egg-info  */*.pyc
	$(ENVON); pip uninstall -y $(NAME) || true

distclean:
	rm -rf $(env_path)

test: $(env_init)
	$(ENVON); tox
